
require 'sinatra/base'
require 'sinatra/reloader'
require 'dotenv'
require 'json'


class App < Sinatra::Base

    Dotenv.load
    register Sinatra::Reloader
    set :root, File.dirname(__FILE__)

#Routes
    

    get '/' do
        erb :index    
        
    end

    get '/todo/item' do
        File.read(ENV['file_save'])
    end 

    post '/todo/item' do

        data_raw    = File.read(ENV['file_save'])
        data        = JSON.parse(data_raw)
        
        newItem = params[:item]
  
        newItem = {
            "name"          => newItem,
            "completed"     => false,
            "uuid"          => SecureRandom.alphanumeric(64)
        }

        data.push(newItem)

        File.open(ENV['file_save'], 'w'){|file| file.write(data.to_json)}

        newItem.to_json
        
    end

    delete '/todo/item' do
        removeID = params['uuid']

        data_raw    = File.read(ENV['file_save'])
        data        = JSON.parse(data_raw)

        test = Array.new(100)

        data.delete_if { |elem| elem['uuid'] == removeID }

        File.open(ENV['file_save'], 'w'){|file| file.write(data.to_json)}

        'true'

    end

    not_found do
        status 404
        'Page not found'
    end

    # run! if app_file == $0
end