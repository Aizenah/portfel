# Подключение gems
require 'sinatra'
require 'sqlite3'
require 'securerandom'


# Стартовая странциа, создание по шаблону erb, подключение к БД, выгрузка в масив и вывод его на странице в виде таблицы
get '/' do
  begin
    db = SQLite3::Database.open "1.db"
    db.results_as_hash = true
    stm = db.prepare "SELECT * FROM to_do_list"
    rs = stm.execute

    @items = [];

    rs.each do |item|
      @items.push(item)
    end

  ensure
    stm.close if stm
    db.close if db
  end
  erb :index
end
# Обновить инфомацию о записи, если ВСЕ поля заполнены
post '/todo/items/upd' do

  title = params[:title]
  completed = params[:completed]
  
  if title == nil  || title == "" || completed == nil || completed == ""
    redirect "/"
  end
  
  begin
    db = SQLite3::Database.new "1.db"
    db.execute("UPDATE to_do_list SET title = '"+title+"', updated_at = '"+Time.now.getutc.to_s+"',completed = '"+completed+"' WHERE uuid = '"+params[:uuid]+"'")
  rescue ::Exception => e
    puts"Exception occurred"
    puts e
  ensure
    db.close if db
  end

  redirect "/"

end
# Добавить предмет в БД
post '/todo/items' do
  title = params[:title]

  begin
    db = SQLite3::Database.new "1.db"
    db.execute("INSERT INTO to_do_list (id, title, uuid, completed, created_at)
      VALUES (
        null,
        '"+ title +"',
        '"+SecureRandom.alphanumeric(64)+"',
        0,
        '"+Time.now.getutc.to_s+"'
      )")

  rescue ::Exception => e
    puts "Exception occurred"
    puts e
  ensure
    db.close if db
  end
  redirect "/"
end
# Удалить предмет из БД
post '/todo/items/delete' do
  uuid = params[:uuid]

  begin
    db = SQLite3::Database.new "1.db"
    db.execute("DELETE FROM to_do_list WHERE uuid='"+uuid+"'")

  rescue ::Exception => e
    puts "Exception occurred"
    puts e
  ensure
    db.close if db
  end
  redirect "/"
end
