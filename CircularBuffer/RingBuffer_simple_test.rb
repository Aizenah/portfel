require_relative  'RingBuffer'
puts 'Начнем. Допустим размер буфера равен 5 единицам.'
@test = RingBuffer.new(5)
puts 'Проверим количество свободного места... Сейчас свободно: ' + @test.getAvailableSpace.to_s + ' единиц'

puts 'Введем в буфер новый предмет (число 1).'
@test.put(1)

puts 'Проверим количество свободного места... Сейчас свободно: ' + @test.getAvailableSpace.to_s + ' единиц'

puts 'Продолжаем заполнять наш цикличный буфер, продолжим введить в буфер числа (2, 3, 4, 5).'

@test.put(2)
@test.put(3)
@test.put(4)
@test.put(5)

puts 'Проверим количество свободного места... Сейчас свободно: ' + @test.getAvailableSpace.to_s + ' единиц'
puts 'На данном этапе буфер полон и сделать запись без извлечения даных из ячейки - невозможно, но давайте проверим.'
if @test.put(6)
  puts 'У Вас получилось невозможное...'
else
  puts 'Увы, буфер заполнен полностью, давайте проверим количество данных доступных для извлечения чтобы мы могли освободить пространство для наших будущих переменных.'
end
puts 'Сейчас для извлечения в буфере доступно: ' + @test.getOccupiedPlace.to_s + ' елементов. '
puts 'Изьяв из буфера хотябы 1 елемент (начинаем мы извлекать с конца, тоесть мы извлечем первый елемент - 1)'
puts 'Мы изьяли: ' +  @test.get.to_s
puts 'У нас остались для заполнения: ' + @test.getAvailableSpace.to_s + ' элемент.'
puts 'На данном этапе буфер НЕ полон и сделать запись возможно, давайте проверим.'
if @test.put(6)
  puts 'У Вас получилось! Теперь на месте первого элемента сияет новоприбывший. Давайте в этом убедимся.'
else
  puts 'Houston we have a problem...'
end
puts 'Теперь у нас доступны: ' + @test.data.to_s
puts 'Готово.'