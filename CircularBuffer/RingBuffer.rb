class RingBuffer
    attr_reader :readIndex, :writeIndex, :isEmpty, :isFull, :bufferSize, :data

    def initialize(buffSize)
        @bufferSize = buffSize

        @readIndex  = 0
        @writeIndex = 0

        @isEmpty    = true
        @isFull     = false

        @data       = Array.new(@bufferSize)
    end
    # Добавить элемент в масив, с последующей проверкой и изменениями во флагах состояния буфера
    def put(newVal)

        if @isFull
            return false
            # return EXCEPTION RingBuffer is FULL
        end



        if @isEmpty

            @isEmpty = false

        end

        @data[@writeIndex] = newVal

        @writeIndex = @writeIndex + 1


        if @writeIndex >= @bufferSize
            @writeIndex = 0
        end

        if @writeIndex == @readIndex
            @isFull = true
        end

        return true
        # return TRUE
    end
    # Поучить крайний элемент в масиве, с последующей проверкой и изменениями во флагах состояния буфера
    def get

        if @isEmpty
            return nil
            # return EXCEPTION TokenRing is Empty
        end

        @isFull = false




        @result = @data[@readIndex]


        @readIndex = @readIndex + 1

        if @readIndex >= @bufferSize
            @readIndex = 0
        end

        if @readIndex == @writeIndex
            @isEmpty = true
            clear
        end

        return @result

    end
    # Поучить к-во свободного места
    def getAvailableSpace

        if @isFull
            return 0
        elsif @isEmpty
            return @bufferSize
        else

            if @writeIndex > @readIndex
                return @bufferSize - (@writeIndex - @readIndex)
            elsif @writeIndex < @readIndex
                return @readIndex - @writeIndex
            else
                # Iimpossible logical situation
            end
        end

    end
    # Поучить к-во занятого места
    def getOccupiedPlace
        return @bufferSize - getAvailableSpace
    end
    # Очистить буфер, вернуть флаги в исходное состояние
    def clear
        @isEmpty    = true
        @isFull     = false
        @readIndex  = 0
        @writeIndex = 0
        @data       = Array.new(@bufferSize)
    end
    end
